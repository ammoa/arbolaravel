<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class date_1_3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:date_1_3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $now = Carbon::now();
        print($now." now  ");
        $after_month = now()->addMonths(1);
        print($after_month." after month  ");
        $days_left = Carbon::CreateFromDate(null ,12 ,31 , null)->diffInDays($now);
        print($days_left." days left until new year  ");

    }
}
