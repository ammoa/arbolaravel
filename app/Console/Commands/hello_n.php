<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class hello_n extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:hello_n {user=dude}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command will print greet user';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $user = $this->argument('user');
        print("hello ".$user);
    }
}
