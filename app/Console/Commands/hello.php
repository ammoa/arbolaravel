<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class hello extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:hello';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command will print hello world';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
        print("hello world <3");
    }
}
